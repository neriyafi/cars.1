@extends('layouts.app')
@section('content')

<h1> Create a new car </h1>
<form method = 'post' action = "{{action('CarController@store')}}"  >
{{csrf_field()}}

<div class = "form-group">
    <label for = "brand"> car brand: </label>
    <input type = "text" class = "form-control" name = "brand">
    <label for = "year"> car year </label>
    <input type = "text" class = "form-control" name = "year">
    <label for = "price"> car price </label>
    <input type = "number" class = "form-control" name = "price">
</div>

<div class = "form-group">
 <input type = "submit" class= "form-control" name="submit" value= "save">
</div>

<ul>
@foreach($errors->all() as $error)
   <li>{{$error}}</li>
@endforeach
</ul>

</form>

@endsection