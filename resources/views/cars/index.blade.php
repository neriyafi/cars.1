<head>

<style>
    h1 {
      color: white;
      text-align: center;
      text-decoration: underline;
    }
    .form-1{
      width:10%;
      margin-left:50%;
      margin-right:50%
    }
    h3 {
      color: white;
      text-align: center;
    }
          table {
            margin-top: 50px;
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }

          td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
            
          }

          tr:nth-child(even) {
            background-color: #dddddd;
      
          }
          tr:nth-child(odd){
            background-color: #aaaaaa;
      
          }
          tr:nth-child(even):hover {
            background-color: gray;
      
          }
          tr:nth-child(odd):hover {
            background-color: gray;
      
          }

</style>
</head>

@extends('layouts.app')
@section('content')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<body>
<h1>This is the car list</h1>


<!-- @if (Request::is('cars'))  
<h2><a href="{{action('CarController@LowtoHigh')}}"> low to high </a></h2>
@endif
@if (Request::is('LowtoHigh'))
<h2><a href="{{action('CarController@HigetoLow')}}"> high to low </a></h2>
@endif
@if (Request::is('HigetoLow'))
<h2><a href="{{action('CarController@LowtoHigh')}}"> low to high </a></h2>
@endif -->

        

                  <table>
                      <tr>
                        <th>ID</th>
                        <th>Brand</th>
                        <th>year</th>
   @cannot('saller')    <th>price
        @if (Request::is('cars'))  <a href="{{action('CarController@LowtoHigh')}}"> sort from low to high </a> @endif
        @if (Request::is('LowtoHigh'))<a href="{{action('CarController@HigetoLow')}}">sort from high to low </a> @endif
        @if (Request::is('HigetoLow')) <a href="{{action('CarController@LowtoHigh')}}">sort from low to high </a>@endif
        @if (Request::is('search')) <a href="{{action('CarController@LowtoHigh')}}">sort from low to high </a>@endif
        @if (Request::is('avg'))<a href="{{action('CarController@HigetoLow')}}">sort from high to low </a> @endif
        </th> @endcannot
          @can('saller')<th>price </th> @endcan
          @can('saller')<th>user_is</th> @endcan
          @can('saller')<th>action</th>  @endcan
                      </tr>
                              
                    

            @foreach($cars as $car)
            <tr>
            <td>{{$car->id}}</td>
           <td><a href="{{route('cars.edit',$car->id)}}"> {{$car->brand}}</a></td>   
             <td>{{$car->year}}</td>
             <td>{{$car->price}}</td>
  @can('saller')   <td>{{$car->user_id}}</td>
             <td><button id ="{{$car->id}}" value="{{$car->brand}} - on hold"> on hold!</button> </td> @endcan
            </tr>
            @endforeach
     </table>
     
     
     @can('saller')   <a href = "{{route('cars.create') }}"> <h1> add a new car </h1> </a> @endcan


 @cannot('saller')
             <h3> <a href="{{action('CarController@avg')}}"> averege price: </a> <h3>
              @if (Request::is('avg')) <h3> averege price is:   {{$avg}} </h3>    @endif    @endcannot

<br><br>
  
@cannot('saller') 
 <form  class = "form-1" method = 'post' action = "{{action('CarController@search')}}"  >
          @csrf
          @method('get')
          <div class = "form-group">
              <label for = "title"> budget from: </label>
              <input type = "text" class = "form-control" name = "budget_from" >
          </div>
          <div class = "form-group">
              <label for = "title"> budget to: </label>
              <input type = "text"  class = "form-control" name = " budget_to" >
          </div>
          <div class = "form-group">
          <input type = "submit"  name="submit" value= "search">
          </div>

</form>   @endcannot
</body>


<script>
       $(document).ready(function(){
           $("button").click(function(event){
               $.ajax({
                   url:  "{{url('cars')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type:  'put',
                   contentType: 'application/json',
                   //data: JSON.stringify({'status':(event.target.value-1)*-1, _token:'{{csrf_token()}}'}), change between 1 and 0
                   data: JSON.stringify({'brand':(event.target.value), _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
               location.reload();

           });

       });
   </script>
  
@endsection