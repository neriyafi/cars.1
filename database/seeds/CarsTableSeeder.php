<?php

use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert(
            [
                [
                    'id' => '4',
                    'brand' => 'Ford',
                    'year'=>'2005',
                    'user_id'=>'9',
                    'price'=>'20000',
                    'created_at' => date('Y,m,d G:i:s'),   
                ],
                [
                    'id' => '5',
                    'brand' => 'mazda',
                    'year'=>'2019',
                    'user_id'=>'9',
                    'price'=>'100000',
                    'created_at' => date('Y,m,d G:i:s'),   
                ],
                [
                    'id' => '6',
                    'brand' => 'mazda',
                    'year'=>'2008',
                    'user_id'=>'9',
                    'price'=>'30000',
                    'created_at' => date('Y,m,d G:i:s'),   
                ],
            ]);
    }
}
