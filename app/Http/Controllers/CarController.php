<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Car;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Collection;
class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        if (Gate::denies('saller')) {
            $id = Auth::id();
            $user = User::Find($id);
            $cars = Car::All();
            return view('cars.index', ['cars' => $cars]);
        }

            $id = Auth::id();
            $user = User::Find($id);
            $cars = $user->cars;;
         //   return view('cars.index', ['cars' => $cars]);  
            return view('cars.index', compact('cars'));
         
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('saller')) {
            abort(403,"Sorry you are not allowed to create");
        }
    return view('cars.create');
             
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            
        if (Gate::denies('saller')) {
            abort(403,"Sorry you are not allowed");
        }
        $this->validate($request, [
            'brand' => 'required',
            'year' => 'required',
            'price' => 'required',
        ]);
             $cars = new Car();   
            $cars->brand = $request->brand; 
            $cars->year = $request->year;
            $cars->price = $request->price;
            $cars->user_id = Auth::id();
            $cars->created_at = null;
            $cars->updated_at = null;
            $cars->save();
            return redirect('cars');
                }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('saller')) {
            abort(403,"Sorry you are not allowed to edit ");
        }  
            $cars = Car::find($id);
            return view('cars.edit', compact('cars'));     
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Gate::denies('saller')) {
            abort(403,"Sorry you are not allowed to update");
        }
            $cars = Car::findOrFail($id);
       // לא עובד if(!$book->user->id == Auth::id()) return(redirect('books'));
        //test if title is dirty
        //$task->update($request->except(['_token']));
      
        $cars-> update($request->all());
       // $task->status = update($request->status);
        
            return redirect('cars');           
        }
   

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('saller')) {
            abort(403,"Sorry you are not allowed");
        }
     
            if (Auth::check()) {
                $cars = Car::find($id);
                $cars->delete();
                return redirect('cars');
                
                }
                return redirect()->intended('/home');
         
        }
       
        // sorted method
        public function LowtoHigh()
        {
            $sorted = Car::All();
            $cars = $sorted->sortBy('price');
        // if i want to sort from high to low
          //  $cars = $sorted->sortByDesc()('price');
            return view('cars.index', compact('cars'));
        }

        public function HigetoLow()
        {
            $sorted = Car::All();
            $cars = $sorted->sortByDesc('price');
            return view('cars.index', compact('cars'));
        }
    
        public function avg()
        {
            $cars = Car::All();
            $avg =  $cars->avg('price');
            return view('cars.index', compact('cars','avg'));
        }

        public function search(Request $request)
        {
            $allcars = Car::All();
            $cars = $allcars->whereBetween('price', [$request->budget_from, $request->budget_to]);
            return view('cars.index', compact('cars'));
        }
    
   
}
